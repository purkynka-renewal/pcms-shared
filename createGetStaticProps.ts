import { GetStaticProps } from "next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import type { SSRConfig } from "next-i18next";
import { PageData, Models, fetchPageData } from "./server";

export type PageProps<TData> = PageData<TData> & SSRConfig;

export function createGetStaticProps<TModelName extends keyof Models>(
  model?: TModelName,
  id?: string,
  langNamespaces?: string[],
): GetStaticProps<PageProps<Models[TModelName] | null>> {
  return async function getStaticProps({ locale }) {
    const [data, lang] = await Promise.all([
      fetchPageData(model, id),
      serverSideTranslations(locale!, ["common", ...(langNamespaces ?? [])]),
    ]);

    return { props: { ...lang, ...data } };
  };
}
