import { extendTheme, ThemeConfig, ColorHues, ThemeOverride, Theme } from "@chakra-ui/react";
import { lighten, darken, mode } from "@chakra-ui/theme-tools";

const config: ThemeConfig = {
  initialColorMode: "system",
  // useSystemColorMode: true,
};

export const colors: Record<string, Partial<ColorHues> | string> = {
  pgreen: getScheme("#199F58"),
  pred: getScheme("#E2001A"),
  pblue: getScheme("#003E90"),
  pyellow: "#EACA24",
  bgLight: "#FFFFFF",
  bgDark: "#171923",
};

const themeOverride: ThemeOverride = {
  config,
  colors,
  shadows: {
    outline: `0 0 0 3px ${colors.pgreen["700"]}`,
  },
  fonts: {
    heading: '"Open Sans", "Helvetica Neue", Helvetica, Arial, system-ui, sans-serif',
    body: '"Open Sans", "Helvetica Neue", Helvetica, Arial, system-ui, sans-serif',
  },
  styles: {
    global: (props) => ({
      body: {
        backgroundColor: mode("bgLight", "bgDark")(props),
      },
      code: {
        wordBreak: "break-word",
      },
    }),
  },
};

export const theme: Theme = extendTheme(themeOverride, {
  colors: {
    bg: mode("white", "black"),
  },
}) as any;

function getScheme(color: string): Partial<ColorHues> {
  const hues: (keyof ColorHues)[] = [50, 100, 200, 300, 400, 500, 600, 700, 800, 900];
  const hueMidI = hues.indexOf(500);
  return hues.reduce((acc, w, i) => {
    const value = ((hueMidI - i) / hues.length) * (100 * 0.6);
    acc[w] = (value > 0 ? lighten(color, -value) : darken(color, value))({});
    return acc;
  }, {} as ColorHues);
}
