import { IconDefinition, IconName } from "@fortawesome/fontawesome-common-types";

export type { IconName } from "@fortawesome/fontawesome-common-types";
export type IconPackName = "regular" | "brands" | "solid";

export default async function fetchFaIcon(icon: IconName, pack: IconPackName = "solid"): Promise<IconDefinition> {
  icon = (icon[0].toUpperCase() + icon.slice(1).replace(/\-([a-z])/g, (_, s) => s.toUpperCase())) as IconName;
  return import(`@fortawesome/free-${pack}-svg-icons/fa${icon}.js`).then((i) => i.definition);
}
