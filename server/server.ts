import type * as Types from "../types";

export type Models = {
  site: Types.Site;
  school: Types.School;
  post: Types.Post;
  course: Types.Course;
};

export type ImageConfig = {
  sizes: number[];
  exts: string[];
  defaultExt: string;
  keepExts: string[];
};

export type PageData<TData> = {
  site: Types.Site;
  school: Types.School;
  imageConfig: ImageConfig;
  page: TData;
};

export function getBackendURL(path = "") {
  return `${process.env.NEXT_PUBLIC_BACKEND_URL}${path}`;
}

async function fetchServer(url: string): Promise<any | null> {
  const response = await fetch(getBackendURL(url));
  if (response.status != 200) return null;
  return response.json();
}

export async function fetchPageData<TModelName extends keyof Models>(
  model?: TModelName,
  id?: string,
): Promise<PageData<Models[TModelName] | null>> {
  const [imageConfig, school, site, page] = await Promise.all([
    fetchServer("/images"),
    fetchServer(`/content/school/1`),
    fetchServer(`/content/site/${process.env.NEXT_PUBLIC_SITE_ID ?? 1}`),
    model ? fetchServer(`/content/${model}/${id ?? ""}`) : null,
  ]);

  return {
    site: { ...site, menu: JSON.parse(site.menu) },
    school,
    imageConfig,
    page,
  };
}

export function getFileURL(file: Types.FileRecord, thumbnail?: boolean) {
  if (thumbnail && file.media && file.media.type === "VIDEO" && !file.media.thumbnail) return;
  return getBackendURL("/files/" + ((thumbnail && file.media && file.media.thumbnail) || file.id + "." + file.ext));
}

export function getImage(fileId: string, size: number, ext?: string) {
  return getBackendURL("/images/" + size + "/" + fileId + "." + (ext ?? "webp"));
}
