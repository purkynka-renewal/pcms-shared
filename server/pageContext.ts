import { createContext } from "react";
import { PageData } from "./server";

export type { PageData } from "./server";

const PageContext = createContext<PageData<any>>({} as PageData<any>);
export default PageContext;
