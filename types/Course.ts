import type { MediaRecord } from "./filesystem";

export interface Course {
  slug: string;
  name: string;
  icon: string;
  slogan: string | null;
  maturita: boolean;

  description: string | null;

  gallery?: MediaRecord[];
}
