import { Site } from "./Site";
import { School } from "./School";
import { Post } from "./Post";

export type FolderRecord = {
  id: number;
  name: string;
  files: FileRecord[];
  folders: FolderRecord[];

  parent: FolderRecord | null;
  parentId: number | null;
};

export type FileRecord = {
  id: string;
  name: string;
  ext: string;
  mime: string;
  size: number;
  media: MediaRecord | null;

  folder: FolderRecord | null;
  folderId: number | null;

  attachmentOf: Post[];
};

export type MediaRecord = {
  file: FileRecord;
  id: string;
  type: "IMAGE" | "VIDEO";
  width: number;
  height: number;
  length: number | null;
  label: string | null;
  thumbnail: string | null;

  inGalleryOf: Post[];
  faviconOf: Site[];
  logoOf: School[];
};

export type FileNodeRecord = FileRecord | FolderRecord;
