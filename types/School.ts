import type { MediaRecord } from "./filesystem";

export interface School {
  id: number;
  name: string;
  shortname: string;

  address: string | null;
  phone: string | null;
  email: string | null;
  dataBox: string | null;

  description: string | null;

  logo?: MediaRecord | null;

  updatedAt: Date | string;
}

export interface Social {
  type: string;
  name: string | null;
  uri: string;

  of: School;
  ofId: number;
}
