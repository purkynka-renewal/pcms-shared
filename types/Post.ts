import type { MediaRecord } from "./filesystem";

export interface Post {
  slug: string;
  title: string;
  content: string | null;

  attachments?: MediaRecord[];
  gallery?: MediaRecord[];

  createdAt: Date | string;
  updatedAt: Date | string;
}
