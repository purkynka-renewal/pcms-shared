import type { MediaRecord } from "./filesystem";
import type { Menu } from "./Menu";

export interface Site {
  id: number;

  menu: Menu;

  favicon?: MediaRecord | null;

  updatedAt: Date | string;
}
