import type { IconName, IconPackName } from "~/fetchFaIcon";

export type Menu = MenuItem[];

export interface MenuItem {
  label: string;
  link: string;
  external?: boolean;
  icon?: IconName | [IconPackName, IconName];
  children?: MenuItem[];
}
